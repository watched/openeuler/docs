# Installing secGear


## OS

openEuler 21.03, openEuler 20.03 LTS SP2, or later

## CPU Architecture

#### x86_64

The Intel Software Guard Extensions (Intel SGX) function is required.

#### AArch64

   - Hardware

      | Item  | Version                                         |
      | ------ | --------------------------------------------- |
      | Server| TaiShan 200 server (model 2280, dual sockets)|
      | Mainboard  | Kunpeng board                                     |
      | BMC    | 1711 board (model BC82SMMAB)                    |
      | CPU    | Kunpeng 920 processor (model 7260, 5250, or 5220)        |
      | Chassis  | No special requirements; an 8- or 12-drive chassis recommended                          |

      The TrustZone feature, including the iTrustee secure OS, BMC firmware, and BIOS firmware, has been installed on the server.

   - Environment Requirements

      The patch in the rich execution environment (REE) is required by a client application (CA) to communicate with a trusted application (TA) in the trusted execution environment (TEE). Before installing secGear, set up the environment as follows:

      1. [Setting Up the TA and CA Operating Environment](https://support.huaweicloud.com/intl/en-us/dpmg-tz-kunpengcctrustzone/kunpengtrustzone_04_0007.html)

      2. Applying for a TA Developer Certificate in a Debugging Environment

         a. [Creating a configs.xml File](https://support.huaweicloud.com/intl/en-us/dpmg-tz-kunpengcctrustzone/kunpengtrustzone_04_0009.html#section0)

         b. [Applying for a TA Developer Certificate](https://support.huaweicloud.com/intl/en-us/dpmg-tz-kunpengcctrustzone/kunpengtrustzone_04_0009.html#section1)

## Installation Guide

To use the secGear confidential computing programming framework, you need to install the secGear and secGear-devel development packages. Before the installation, ensure that the openEuler Yum source has been configured.

1. Install the secGear component with **root** permissions:

   ```shell
   #yum install secGear
   #yum install secGear-devel
   ```

2. Run the following commands to check whether the installation is successful. If the command output contains the corresponding software package, the installation is successful.

   k
   #rpm -q secGear 
   #rpm -q secGear-devel
   ```
