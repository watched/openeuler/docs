# NestOS User Guide

This document describes the installation, deployment, features, and usage of the NestOS cloud-based operating system. NestOS integrates mainstream container platforms, such as iSulad, Docker, and PodMan. It eliminates unreliable issues during upgrades caused by modification to the system, dependency of user services on system components, and unstable intermediate states of software packages. NestOS is provided as a lightweight and customizable OS for easy cluster deployment. To download NestOS images, visit the [NestOS Repository](https://gitee.com/openeuler/NestOS).
