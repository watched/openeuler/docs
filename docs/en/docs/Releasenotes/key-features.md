# Key Features

## Innovations Based on Linux Kernel 5.10

In-depth optimizations of scheduling, I/O, and memory management have been performed, and various computing power, such as ARM64, x86, and RISC-V is supported.

- **Dynamic kernel preemption**: The new boot option **preempt=none/voluntary/full** is added to allow the kernel to dynamically switch the preemption mode.

- **mremap performance optimization**: Entries at the PMD and PUD levels can be moved to accelerate the speed of mapping large memory blocks.

- **per memcg lru lock**: It alleviates lock contention between cloud native container instances and improve system performance.

- **Huge page memory management optimization**: The tail pages among HugeTLB huge pages are released in shared mapping mode to reduce the overhead and memory usage incurred in managing the huge page memory.

- **Concurrent translation lookaside buffer (TLB) updates**: The local TLB and remote TLB can be updated at the same time to optimize the TLB shootdown process. The benefits are quicker TLB updates and improved service performance.

- **Huge page vmalloc performance optimization**: When calling vmalloc() to allocate spaces that exceed the minimum size of huge pages, the huge page instead of the base page is used to map the memory, improving the TLB usage and reducing TLB misses.

- **Uncorrectable error (UCE) fault tolerance**: When a UCE occurs in the copy_from_user scenario, the system kills the associated user-mode process instead of kernel panicking. This feature is disabled by default. You can enable it by adding **CONFIG_UCE_KERNEL_RECOVERY** to the kernel configuration, and configure it in the kernel boot parameter interface **/proc/cmdline** (add **uce_kernel_recovery=[0,4]**) and proc interface **/proc/sys/kernel/uce_kernel_recovery**.

## File System for New Media

- Eulerfs has the innovative metadata soft update technology. Its pointer-based directory dual-view counting mechanism reduces metadata synchronization overheads and effectively improves the call performance of create, unlink, mkdir, and rmdir functions in the file system. 


## Tiered Memory Expansion

Various memory and storage media can be used to expand the system memory capacity and reduce the memory usage. User-mode swap is supported.

- **User-mode swap**: The evicted cold memory can be swapped to the user-mode storage based on a preset etMem policy. The user-mode swap delivers a higher performance than the kernel-mode swap and the whole swap process is transparent to users.

## User-Mode Protocol Stack
The Gazelle user-mode protocol stack is added. It can be used without application program modification or recompilation, supporting high-performance and low-latency network transmission for upper-layer services.

- **Gazelle user-mode protocol stack**: Powered by dpdk and lwip, Gazelle is a lock-free and multi-thread high-performance user-mode protocol stack to improve the network performance of applications. It can be used without modification, adaptation, or recompilation.

## Enhanced Cloud Native Scheduling

In cloud service scenarios, online interactive services are sensitive to latencies and have a tidal effect. Their CPU usage is generally less than 15%. A hybrid deployment of online and offline services is an effective way to improve resource utilization.

- **Quality Aware Scheduler (QAS)**: It ensures that online tasks quickly preempt CPU resources, schedules tasks in a deterministic manner, and suppresses interference from offline tasks.
- **Memory reclamation for OOM**: When OOM occurs, memory reclamation is preferentially performed for process groups with low priorities to ensure the normal running of online services.
- **Hybrid container deployment framework**: In a hybrid Kubernetes cluster, openEuler users only need to add online or offline labels to services. The system can automatically detect service creation and configure resources based on service priorities to implement resource isolation and preemption.

## QEMU Hot Patch
- **LibcarePlus**: It is a hot patch framework that can fix QEMU process bugs without affecting VM services.

## KubeOS

KubeOS is a container operating system, which centrally manages cloud native cluster OSs in containers. It has the following features:

- **OS containerization**: Interconnection with Kubernetes facilitates unified OS management and atomized lifecycle management.
- **Lightweight OS tailoring**: This reduces unnecessary packages for quicker upgrades and replacements.


## Enhanced Lightweight Secure Container

Based on Stratovirt, a lightweight virtualization technology, containers have low load and VMs are more secure.

-  **UEFI boot**: Supports UEFI boot, ACPI table construction, and addition of PCIe/PCI devices (including vrtio-pci) to VMs.
-  **VFIO**: It allows physical devices on the host to be accessed from VMs and enables VMs to obtain high performance close to raw devices.
-  **Hot swap of passthrough devices**: Devices such as virtio-blk-pci, virtio-net-pci, and VFIO can be hot swapped. This prevents system shutdown and service interruption caused by peripheral replacement.

## Enhanced iSulad

- The shimv2 process incorporates the kata-runtime,kata-shim, kata-proxy processes, and the RPC is invoked to process various containers during the runtime. Lifecycle management commands are used to simplify the system architecture. 

## Dual-Plane Deployment

eggo is a Kubernetes cluster deployment and management project of the openEuler cloud native special interest group (SIG). It provides efficient and stable cluster deployment capabilities.

-  **Version-based cluster configuration management**: Git repositories are used to store and track cluster configuration changes.
-  **x86/ARM dual planes**: implements unified cluster deployment, monitoring, and audit of OSs.

## Edge Computing

Unified edge-cloud synergy framework KubeEdge+ is provided, implementing basic capabilities such as edge-cloud application management and deployment and edge-cloud communication.

- **Management collaboration**: Devices in a single cluster can be managed in a unified manner and applications can be sent in seconds.
- **Network collaboration**: The edge-cloud bidirectional communication and communication between edge nodes in private subnets are supported.
- **Edge autonomy**: Edge autonomy is supported to ensure that edge nodes work properly when the network is unstable. Metadata persistence and quick recovery of edge nodes are supported.
- **Lightweight edge**: It occupies less memory and can work properly if resources are limited.

## Embedded Image

- ** Lightweight models**: The Yocto small-scale build tailoring framework allows you to customize system items into compressed, lightweight models. For example, OS images can be reduced to less than 5 MB, memory overhead to under 15 MB, and startup to under 5s.
- **Support for multiple types of hardware**: Raspberry Pi 4B is supported as the universal hardware for embedded scenarios.
- **Soft real-time kernel**: The soft real-time capabilities based on the Linux 5.10 kernel allow for microsecond-level soft real-time interrupt response latency.
- **Hybrid critical deployment**: Hybrid deployment of real-time and non-real-time planes in the SOC is supported. Zephyr real-time kernel is also supported.
- **Distributed soft bus (DSoftBus)**: The DSoftBus of HarmonyOS is integrated to implement interconnection and interworking between embedded devices running on openEuler.
- **Embedded software package support**: More than 80 common embedded software packages can be built using openEuler.


## secPaver
secPaver is an SELinux policy development tool used to assist developers in developing security policies for applications.
- **Policy management**: provides a high-level configuration language and generates SELinux policy files based on the policy configuration file to reduce the threshold for using SELinux.

## NestOS
NestOS is a cloud-based OS provided by the CloudNative SIG of the openEuler community. It is dedicated to providing optimal container hosts for securely running containerized workloads on a large scale.
- **Out-of-the-box container platforms**: Mainstream basic container platforms such as iSulad, Docker, PodMan, and CRI-O are included.
- **Easy-to-use installation and configuration**: Ignition is used to provide personalized configuration.
- **Secure and reliable package management**: rpm-ostree is used for package management.
- **User-friendly and controllable automatic update agent**: Zincati provides insensible upgrade.
- **Dual-partition system**: The dual-partition design ensures system security.

## Third-party Application Support

- **KubeSphere**: It is an application-centric and open-source container platform built based on Kubernetes. It is initiated by Beijing Qingyun Technology Co., Ltd. and supported and maintained by SIG-KubeSphere of the openEuler community.
- **OpenStack Wallaby**: OpenStack is updated to the latest Wallaby version. Wallaby is released in April 2021, including important updates of core projects such as Nova, Kolla, Cyborg, and Tacker.
- **OpenResty**: It is a high-performance web platform built based on Nginx and Lua.

## Desktop Environments

More desktop environments are provided to ensure better development experience.

- **DDE** is upgraded and supports drawing board, music, and cinema applications.
- **UKUI** is upgraded and supports the Chinese input method and multimedia.
- **kiran-desktop** is supported.
- **GNOME** is supported.
