# Common Vulnerabilities and Exposures (CVEs)<a name="EN-US_TOPIC_0228206866"></a>

For details about the CVEs involved in the version, see the [CVE list](https://www.openeuler.org/en/security/cve/).

