# User Notice<a name="EN-US_TOPIC_0232332818"></a>

-   The version number counting rule of openEuler is changed from openEuler  _x.x_  to openEuler  _year_._month_. For example, openEuler 21.03 indicates that the version is released in March 2021.
-   The [Python core team](https://www.python.org/dev/peps/pep-0373/#update) has stopped maintaining Python 2 in January 2020. Python 2 reached end of maintenance (EOM) on December 31, 2020. In 2021, openEuler 21.03 fixed only the critical CVEs related to Python 2. Please switch to Python 3 as soon as possible.
-   From openEuler 22.03 LTS, only Python 3 is supported. Please switch to Python 3 to use the OS.

