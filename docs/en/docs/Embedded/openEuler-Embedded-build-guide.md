openEuler Embedded Build Guide
=====================

This document provides two methods for preparing a building environment and describes the building process in details. Choose one of the methods as required.

- To build openEuler Embedded, see [Quick Build Guide](./quick-build-guide.md)
- For containerized build, see [Container Build Guide](./container-build-guide.md)