Quick Build Guide
=====================

This chapter describes how to build openEuler Embedded.

<!-- TOC -->
  - [Environment Preparation](#environment-preparation)
    - [Using Host Machine Commands in Yocto](#using-host-machine-commands-in-yocto)
    - [Tools Required for Building openEuler Embedded](#tools-required-for-building-openeuler-embedded)
    - [Build Container with Tools Installed](#build-container-with-tools-installed)
  - [Version Build](#version-build)
    - [Downloading Build Code](#downloading-build-code)
    - [Compiling the Build](#compiling-the-build)
    - [Build Result](#build-result)
    <!-- /TOC -->

Environment Preparation
--------------

### Using Host Machine Commands in Yocto

Yocto, or BitBake, is a group of Python programs. The minimum environment requirements are as follows:

- Python3 \> 3.6.0
- Git \> 1.8.3.1
- Tar \> 1.28

Yocto can build other tools required in the build process based on the corresponding software package recipes so that Yocto is self-contained. During this process, Yocto installs dedicated Yocto patches for corresponding tools (such as DNF and RPM) as required. These host tools are built from the source code in the first build. Therefore, the first build of Yocto can be time-consuming.

To accelerate the building process, especially for the first build, openEuler Embedded preferentially uses the pre-built native tools on the host. This requires that the software package management tools (such as APT, DNF, Yum, and Zypper) be installed in advance.

Yocto uses the **HOSTTOOLS** variable to introduce host tools and creates a soft link for each tool listed in **HOSTTOOLS**. To prevent host contamination, Yocto prepares an environment that is independent from the host, for example, the **PATH** variable. Therefore, if a command that depends on the host is added, the command must be added to **HOSTTOOLS**. Otherwise, even if the command exists on the host, an error message will be displayed during Yocto building, indicating that the corresponding tool cannot be found. The following figure shows the process.

![](./public_sys-resources/hosttools.png)

The host tools required by openEuler Embedded are defined in **HOSTTOOLS** in the **local.conf.sample** file by default. The tools include:

| Tool| Description         |
| ------ | ------------- |
| cmake  | CMake build tool|
| ninjia | Ninja build system|

### Tools Required for Building openEuler Embedded

- Operating system.

   [openEuler-20.03-LTS-SP2](https://repo.openeuler.org/openEuler-20.03-LTS-SP2/docker_img/x86_64/openEuler-docker.x86_64.tar.xz)

- Additional tools.

```
yum -y install tar cmake gperf sqlite-devel chrpath gcc-c++ patch rpm-build flex autoconf automake m4 bison bc libtool gettext-devel createrepo\_c rpcgen texinfo hostname python meson dosfstools mtools parted ninja-build autoconf-archive libmpc-devel gmp-devel
```

- Pre-built cross-compilation toolchain and library.

   Yocto can build the cross-compilation toolchains and C libraries required for cross-compilation. However, the process is complex and time-consuming and is performed only in the first build. At the same time, most developers do not build toolchains and C libraries themselves. To simplify the process, openEuler Embedded uses pre-built cross-compilation toolchains and libraries maintained by the openEuler community.

   Currently, the toolchains for the ARM32 and AArch64 architectures are provided. You can download the toolchains by performing the following methods:

    -   [ARM32 Toolchain] (https://gitee.com/openeuler/yocto-embedded-tools/attach_files/1003463/download/openeuler_gcc_arm32le.tar.xz)
    -   [AArch64 Toolchain] (https://gitee.com/openeuler/yocto-embedded-tools/attach_files/1003462/download/openeuler_gcc_arm64le.tar.xz)
    - Download the RPM package: `wget https://repo.openeuler.org/openEuler-22.03-LTS/EPOL/main/x86_64/Packages/gcc-cross-1.0-0.oe2203.x86_64.rpm`
    - Decompress the RPM package: `rpm2cpio gcc-cross-1.0-0.oe2203.x86_64.rpm | cpio -id`
    - After the decompression, a **/tmp** directory containing the toolchain is generated in the current directory.
      - Toolchain for the ARM 32-bit architecture: openeuler_gcc_arm32le.tar.xz
      - Toolchain for the ARM 64-bit architecture: openeuler_gcc_arm64le.tar.xz

### Build Container with Tools Installed

A large number of host tools are used during the build of openEuler Embedded. To accelerate the build process, openEuler Embedded uses the tools installed on the host in advance. However, the tool versions vary depending on the host environment. For example, CMake 1.9 or later is required for the build, but the version on the host is CMake 1.8. To solve this problem, openEuler Embedded provides a dedicated build container to provide a unified build environment.

You can obtain the container images for the build from the following link:

​ [Basic Build Container Image for openEuler Embedded](https://repo.openeuler.org/openEuler-21.03/docker_img/x86_64/openEuler-docker.x86_64.tar.xz)

For details, see the [Container Build Guide](./container-build-guide.html).

## Version Build

### Downloading Build Code

The file layout of the openEuler Embedded build project is as follows. Assume that **openeuler\_embedded** is the top-level directory.

><openeuler_embedded>  
>├── src Source code directory, including all software package code, kernel code, and Yocto build code  
>├── build openEuler Embedded build directory. The generated images are stored in this directory.

1. Obtain the source code download script.

     Download the script to a specified directory, for example, **src/yocto-meta-openeuler**.

   ```
   git clone https://gitee.com/openeuler/yocto-meta-openeuler.git -b openEuler-22.03-LTS -v src/yocto-meta-openeuler
   ```

   The script **src/yocto-meta-openeuler/scripts/download\_code.sh** has the following parameters:

   - 1. Path to download the source code. By default, the source code is downloaded relative to the script location. For example, in the preceding example, the code repository is downloaded to the **src/** directory.
   - 2. Branch to be downloaded. See the script for the default value. Determine the branch based on the version.
   - 3. XML file for downloading the code, which is in the standard manifest format. The code is downloaded based on the XML configurations.

2. Download the source code using the script.

   - To download the latest code: 

     ```
     sh src/yocto-meta-openeuler/scripts/download_code.sh
     ```

   - To download the code of a specified version: 

     ```
     sh src/yocto-meta-openeuler/scripts/download_code.sh "" "" "manifest.xml"
     ```

     Obtain the **manifest.xml** file for specifying the version of the openEuler Embedded code from the openEuler Embedded release directory **embedded\_img/source-list/**.

### Compiling the Build

Run the one-click compilation script **src/yocto-meta-openeuler/scripts/compile.sh**. For details, see the script content.

The process of the compilation script is as follows:

1.  Adds the paths of the additional tools to **PATH**.
2.  Sets the **TEMPLATECONF** variable to include the paths of configuration files such as **local.conf.sample**.
3.  Invokes `oe-init-build-env` of the Poky repository to perform initial configuration.
4.  Sets the **MACHINE** variable in the **conf/local.conf** file in the compilation directory to add layers as required.
5.  Runs the `bitbake openeuler-image` command in the compilation directory to build the image and SDK of openEuler Embedded.
6.  The files are generated in the **output** directory of the compilation directory.

Run the compilation script. The following uses the standard ARM architecture as an example to describe the compilation method:

    source src/yocto-meta-openeuler/scripts/compile.sh arm-std
    bitbake openeuler-image # You will be prompted to run the bitbake command after the source command is executed

### Build Result

By default, the files are generated in the **output** directory of the build directory. For example, the built files of the ARM example are generated in the **/usr1/build/output** directory, as shown in the following table:

| filename                                                  | description                         |
| --------------------------------------------------------- | ----------------------------------- |
| Image-\*                                                   | openEuler Embedded image            |
| openeuler-glibc-x86\_64-openeuler-image-*-toolchain-**.sh | openEuler Embedded sdk toolchain    |
| openeuler-image-qemu-aarch64-*.rootfs.cpio.gz             | openEuler Embedded file system      |
| zImage                                                    | openEuler Embedded compressed image |
