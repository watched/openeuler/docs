Container Build Guide
==============================

The openEuler Embedded build process is based on the openEuler OS, and requires many system tools and build tools to be installed. To help developers quickly set up a build environment, the operating system and tools on which the build process depends are encapsulated into a container. In this way, developers can avoid the time-consuming environment preparation process and focus on development.

<!-- TOC -->

  - [Environment Preparation](#environment-preparation)
    - [Installing Docker](#installing-docker)
    - [Obtaining the Container Image](#obtaining-the-container-image)
    - [Preparing the Container Build Environment](#preparing-the-container-build-environment)
  - [Version Build](#version-build)
    - [Downloading Source Code](#downloading-source-code)
    - [Compiling the build](#compiling-the-build)
    - [Build Result](#build-result)
    <!-- /TOC -->

## Environment Preparation

Use Docker to create a container environment. The software and hardware requirements of Docker are as follows:

-   OS: Ubuntu, Debian, and RHEL (CentOS and Fedora) are recommended.
-   Kernel: Linux 3.8 or later is recommended.
-   Driver: The kernel must include a proper storage driver, for example, Device Mapper, AUFS, vfs, btrfs, or ZFS.
-   Architecture: 64-bit architecture (x86-64 or AMD64).

### Installing Docker

-------------

1. Check whether Docker has been installed in the current environment.

Run the following command. If the Docker version is displayed, Docker has been installed in the current environment. You can use it directly.

``` {.sourceCode .console}
docker version
```

1. If Docker is not installed, install it by referring to the [official document](http://docs.docker.com/engine/install/).

Install Docker on openEuler by referring to the installation guide for CentOS.

For example, run the following command to install Docker on openEuler:

``` 
sudo yum install docker
```

### Obtaining the Container Image

---------------

Run the `docker pull` command to pull the image from Huawei Cloud to the host machine:

``` 
docker pull swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container:22.03-lts
```

### Preparing the Container Build Environment

-------------------

#### 1. Start a container.

Run the docker run command to start the container. To ensure that the container can run in the background and access the Internet after being started, you are advised to run the following command to start the container:

``` 
docker run -idt --network host swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container:22.03-lts bash
```

Parameter description:

-   -i: keeps the standard input open.
-   -d: starts a container in daemon mode in the background.
-   -t: allocates a pseudo-tty and binds it to the standard input of the container.
-   \--network: connects the container to the network of the host machine.
-   swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container:22.03-lts (image_name:image_version)
-   bash: method for accessing a container.

#### 2. Check the ID of the started container.

``` 
docker ps
```

#### 3. Enter the container.

``` 
docker exec -it <container_id> bash
```

After the build environment is ready, you can build in the container.

## Version Build

### Downloading Source Code

1. Obtain the source code download script.

``` 
git clone https://gitee.com/openeuler/yocto-meta-openeuler.git -b openEuler-22.03-LTS -v /usr1/openeuler/src/yocto-meta-openeuler
```

2. Download the source code using the script.

``` 
cd /usr1/openeuler/src/yocto-meta-openeuler/scripts
sh download_code.sh /usr1/openeuler/src
```

### Compiling the build

-   Compilation architecture: aarch64-std, aarch64-pro, arm-std or raspberrypi4-64
-   Build directory: **/usr1/build**
-   Source code directory: **/usr1/openeuler/src**
-   Path of the compiler: **/usr1/openeuler/gcc/openeuler\_gcc\_arm64le**

>![](./public_sys-resources/icon-note.gif) **NOTE**  
>- Use different compilers for different compilation architectures. aarch64-std, aarch64-pro, and raspberrypi4-64 use the openeuler\_gcc\_arm64le compiler, and arm-std uses the openeuler\_gcc\_arm32le compiler.
>- The following uses the aarch64-std architecture as an example.

1. Change the owner group of the **/usr1** directory to openeuler. Otherwise, permission issues may occur when switching to the **openeuler** user.

``` 
chown -R openeuler:users /usr1
```

2. Switch to the **openeuler** user.

``` 
su openeuler
```

3. Go to the path where the compilation script is stored and run the script.

``` 
cd /usr1/openeuler/src/yocto-meta-openeuler/scripts
source compile.sh aarch64-std /usr1/build /usr1/openeuler/gcc/openeuler_gcc_arm64le
bitbake openeuler-image
```

### Build Result

By default, the files are generated in the **output** directory of the build directory. For example, the built files of the aarch64-std example are generated in the **/usr1/build/output** directory, as shown in the following table:

| Filename                                                  | Description                         |
| --------------------------------------------------------- | ----------------------------------- |
| Image-\*                                                  | openEuler Embedded image            |
| openeuler-glibc-x86\_64-openeuler-image-*-toolchain-**.sh | openEuler Embedded SDK toolchain    |
| openeuler-image-qemu-aarch64-*.rootfs.cpio.gz             | openEuler Embedded file system      |
| zImage                                                    | openEuler Embedded compressed image |
