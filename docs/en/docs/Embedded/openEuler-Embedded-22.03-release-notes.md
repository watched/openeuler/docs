# openEuler Embedded 22.03 Release Notes

openEuler Embedded 22.03 is the first official release of openEuler Embedded. This section describes the main features of this version.

## Kernel

- The kernel is upgraded to 5.10.0-60.17.0.

- The kernel supports Preempt-RT patches.

- The kernel supports Raspberry Pi 4B patches.

## Software Packages

- More than 80 software packages are supported. For details, see [Supported Software Packages](https://openeuler.gitee.io/yocto-meta-openeuler/features/software_package_description.html).

## Feature Highlights

- The multi-OS hybrid deployment framework supports hybrid deployment of openEuler Embedded and Zephyr. For details, see [Multi-OS Hybrid Deployment Framework](https://openeuler.gitee.io/yocto-meta-openeuler/features/mcs.html).
- The distributed soft bus is preliminarily integrated. For details, see [Distributed Soft Bus](https://openeuler.gitee.io/yocto-meta-openeuler/features/distributed_soft_bus.html).

- For details about security hardening, see [Security Hardening Description](https://openeuler.gitee.io/yocto-meta-openeuler/security_hardening/index.html).
- Preempt-RT-based soft real-time. For details, see [Soft Real-Time System Introduction](https://openeuler.gitee.io/yocto-meta-openeuler/features/preempt_rt.html).

## Southbound Ecosystem

- Added support for Raspberry Pi 4B. For details, see [Support for Raspberry Pi 4B](https://openeuler.gitee.io/yocto-meta-openeuler/features/raspberrypi.html).

## Build System

- openEuler Embedded build system is preliminarily implemented. For details, see [Quick Build Guide](./quick-build-guide.md).
- Containerized build. For details, see [Container Build Guide](./container-build-guide.md).
