# Application Development Using openEuler Embedded SDK

In addition to the basic functions of openEuler Embedded, you can also develop applications, that is, running your own programs on openEuler Embedded. This chapter describes how to develop applications using openEuler Embedded SDK.

<!-- TOC -->
- [Application Development Using openEuler Embedded SDK](#application-development-using-openeuler-embedded-sdk)
    - [Installing the SDK](#installing-the-sdk)
    - [Using the SDK to Build a Hello World Example](#using-the-sdk-to-build-a-hello-world-example)
    - [Using the SDK to Build a Kernel Module Example](#using-the-sdk-to-build-a-kernel-module-example)
<!-- /TOC -->

### Installing the SDK

1.  **Run the self-extracting installation script of the SDK.**

Run the following command:

```
sh openeuler-glibc-x86_64-openeuler-image-aarch64-qemu-aarch64-toolchain-22.03.sh
```

Enter the installation path of the toolchain as prompted. The default path is **/opt/openeuler/\<openeuler version\>/**. You can also set the path to a relative or absolute path.

The following is an example:

``` 
sh ./openeuler-glibc-x86_64-openeuler-image-armv7a-qemu-arm-toolchain-22.03.sh``
openEuler embedded(openEuler Embedded Reference Distro) SDK installer version 22.03
================================================================
Enter target directory for SDK (default: /opt/openeuler/22.03): sdk
You are about to install the SDK to "/usr1/openeuler/sdk". Proceed [Y/n]? y
Extracting SDK...............................................done
Setting it up...SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
$ . /usr1/openeuler/sdk/environment-setup-armv7a-openeuler-linux-gnueabi
```

2.  **Set the environment variable of the SDK.**

Run the `source` command. The `source` command is displayed in the output of the previous step. Run the command.

```
. /usr1/openeuler/myfiles/sdk/environment-setup-armv7a-openeuler-linux-gnueabi
```

3.  **Check whether the installation is successful.**

Run the following command to check whether the installation and environment configuration are successful:

```
arm-openeuler-linux-gnueabi-gcc -v
```

### Using the SDK to Build a Hello World Example

1.  **Prepare the code.**

The following describes how to build a hello world program that runs in the image of the openEuler Embedded root file system.

Create a **hello.c** file. The source code is as follows:

``` c
#include <stdio.h>

int main(void)
{
    printf("hello world\n");
}
```

Compose a **CMakelist.txt** file as follows and place it in the same directory as the **hello.c** file.

```
project(hello C)

add_executable(hello hello.c)
```

2.  **Compile and generate a binary file.**

Go to the directory where the **hello.c** file is stored and run the following commands to compile the file using the toolchain:

```
cmake ..
make
```

Copy the compiled **hello** program to a sub-directory of **/tmp/** (for example, **/tmp/myfiles/**) on openEuler Embedded. For details about how to copy the file, see [Shared File System Enabled Scenario](./installation-and-running.md#shared-file-system-enabled-scenario).

3.  **Run the user-mode program.**

Run the **hello** program on openEuler Embedded.

```
cd /tmp/myfiles/
./hello
```

If the running is successful, the message **hello world** is displayed.

### Using the SDK to Build a Kernel Module Example

1. **Prepare the environment.**

After setting up the SDK, you need to prepare the environment for compiling the kernel module. Run the following commands to create the corresponding kernel module compilation environment:

```
cd <SDK_PATH>/sysroots/<target>-openeuler-linux/usr/src/kernel
make  module_prepare
```

2.  **Prepare the code.**

The following describes how to build a kernel module that runs in the kernel of openEuler Embedded.

Create a **hello.c** file. The source code is as follows:

```c
#include <linux/init.h>
#include <linux/module.h>

static int hello_init(void)
{
    printk("Hello, openEuler Embedded!\r\n");
    return 0;
}

static void hello_exit(void)
{
    printk("Byebye!");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE(GPL);
```

Compose a Makefile as follows and place it in the same directory as the **hello.c** file.

```
 KERNELDIR := ${KERNEL_SRC_DIR}
 CURRENT_PATH := $(shell pwd)

 target := hello
 obj-m := $(target).o

 build := kernel_modules

 kernel_modules:
 		$(MAKE) -C $(KERNELDIR) M=$(CURRENT_PATH) modules
 clean:
 		$(MAKE) -C $(KERNELDIR) M=$(CURRENT_PATH) clean
```

`KERNEL_SRC_DIR` indicates the directory of the kernel source tree. This variable is automatically configured after the SDK is installed.

3.  **Compile and generate a kernel module.**

Go to the directory where the **hello.c** file is stored and run the following command to compile the file using the toolchain:

    make

Copy the compiled **hello.ko** file to a directory on openEuler Embedded. For details about how to copy the file, see the [Shared File System Enabled Scenario](./installation-and-running.md#shared-file-system-enabled-scenario).

4. **Insert the kernel module.**

Insert the kernel module to openEuler Embedded:

    insmod hello.ko

If the running is successful, the message **Hello, openEuler Embedded!** is output to the kernel logs.