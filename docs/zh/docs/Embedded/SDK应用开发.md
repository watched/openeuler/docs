# 基于openEuler Embedded的SDK应用开发

当前发布的镜像除了体验openEuler Embedded的基本功能外，还可以进行基本的应用开发，也即在openEuler Embedded上运行用户自己的程序。本章主要介绍如何基于openEuler Embedded的SDK进行应用开发。

<!-- TOC -->

- [安装SDK](#安装SDK)
- [使用SDK编译hello world样例](#使用SDK编译helloworld样例)
- [使用SDK编译内核模块样例](#使用SDK编译内核模块样例)

  <!-- /TOC -->

###  1. 安装SDK

1. **安装依赖软件包**

   使用SDK开发内核模块需要安装一些必要的软件包，运行如下命令：

   ```
   在 openEuler 上安装:
   yum install make gcc g++ flex bison gmp-devel libmpc-devel openssl-devel
   
   在 Ubuntu 上安装：
   apt-get install make gcc g++ flex bison libgmp3-dev libmpc-dev libssl-dev
   ```

2. **执行SDK自解压安装脚本**

   运行如下命令：

   ```
   sh openeuler-glibc-x86_64-openeuler-image-aarch64-qemu-aarch64-toolchain-22.09.sh
   ```

   根据提示输入工具链的安装路径，默认路径是`/opt/openeuler/<openeuler embedded version>/`。若不设置，则按默认路径安装，也可以配置相对路径或绝对路径。

   举例如下：

   ```
   sh ./openeuler-glibc-x86_64-openeuler-image-armv7a-qemu-arm-toolchain-22.09.sh
   
   openEuler embedded(openEuler Embedded Reference Distro) SDK installer version 22.09
   ================================================================
   
   Enter target directory for SDK (default: /opt/openeuler/22.09): sdk
   You are about to install the SDK to "/usr1/openeuler/sdk". Proceed [Y/n]? y
   Extracting SDK...............................................done
   Setting it up...SDK has been successfully set up and is ready to be used.
   Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
   $ . /usr1/openeuler/sdk/environment-setup-armv7a-openeuler-linux-gnueabi
   ```

3. **设置SDK环境变量**

   运行source命令。第2步执行结束后已打印source命令，直接运行即可。

   ```
   . /usr1/openeuler/myfiles/sdk/environment-setup-armv7a-openeuler-linux-gnueabi
   ```

4. **查看是否安装成功**

   运行如下命令，查看是否安装成功、环境设置成功。

   ```
   arm-openeuler-linux-gnueabi-gcc -v
   ```

###  2. 使用SDK编译hello world样例

1. **准备代码**

   以构建一个hello world程序为例，运行在openEuler Embedded根文件系统镜像中。

   创建一个`hello.c`文件，源码如下：

   ```c
   #include <stdio.h>
   
   int main(void)
   {
       printf("hello world\n");
   }
   ```

   编写`CMakeLists.txt`，和`hello.c`文件放在同一个目录。

   ```
   project(hello C)
   
   add_executable(hello hello.c)
   ```

2. **编译生成二进制**

   进入`hello.c`文件所在目录，使用工具链编译，命令如下：

   ```
   cmake ..
   make
   ```

   把编译好的hello程序拷贝到openEuler Embedded系统的`/tmp/`某个目录下（例如`/tmp/myfiles/`）。如何拷贝可以参考前文所述[使能共享文件系统场景](./安装与运行.html#使能共享文件系统场景)。

3. **运行用户态程序**

   在openEuler Embedded系统中运行hello程序。

   ```
   cd /tmp/myfiles/
   ./hello
   ```

   如运行成功，则会输出\"hello world\"。

###  3. 使用SDK编译内核模块样例

2. **准备代码**

   以编译一个内核模块为例，运行在openEuler Embedded内核中。

   创建一个`hello.c`文件，源码如下：

   ```c
   #include <linux/init.h>
   #include <linux/module.h>
   
   static int hello_init(void)
   {
       printk("Hello, openEuler Embedded!\r\n");
       return 0;
   }
   
   static void hello_exit(void)
   {
       printk("Byebye!");
   }
   
   module_init(hello_init);
   module_exit(hello_exit);
   
   MODULE_LICENSE("GPL");
   ```

   编写`Makefile`，和`hello.c`文件放在同一个目录：

   ```
    KERNELDIR := ${KERNEL_SRC_DIR}
    CURRENT_PATH := $(shell pwd)
   
    target := hello
    obj-m := $(target).o
   
    build := kernel_modules
   
    kernel_modules:
    		$(MAKE) -C $(KERNELDIR) M=$(CURRENT_PATH) modules
    clean:
    		$(MAKE) -C $(KERNELDIR) M=$(CURRENT_PATH) clean
   ```

   > ![](./public_sys-resources/icon-note.gif) **说明：**   
   >
   >  - `KERNEL_SRC_DIR` 为SDK中内核源码树的目录，该变量在安装SDK后会被自动设置。
   >
   >  - `$(MAKE) -C $(KERNELDIR) M=$(CURRENT_PATH) modules`和`$(MAKE) -C $(KERNELDIR) M=$(CURRENT_PATH) clean`代码前均为Tab键，非空格键。

3. **编译生成内核模块**

   进入`hello.c`文件所在目录，使用工具链编译，命令如下：

   ```
   make
   ```

   将编译好的`hello.ko`拷贝到openEuler Embedded系统的目录下。

   如何拷贝可以参考前文所述[使能共享文件系统场景](./安装与运行.html#使能共享文件系统场景)。

4. **插入内核模块**

   在openEuler Embedded系统中插入内核模块:

   ```
   insmod hello.ko
   ```

   如运行成功，则会在内核日志中出现"Hello, openEuler Embedded!"。