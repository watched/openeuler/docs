# openEuler Embedded 22.09发行说明

openEuler Embedded 22.09是openEuler Embedded第二个创新版本，包含的内容大概如下：

## 内核

- 内核升级到 5.10.0-106.8.0
- 内核支持Preempt-RT补丁
- 内核支持树莓派4B相关补丁

## 软件包

- 支持140+软件包，详见[当前所支持的软件包](https://openeuler.gitee.io/yocto-meta-openeuler/features/software_package_description.html)

## 亮点特性

- 多OS混合部署能力增强，新增树莓派4B混合部署实例，新增支持服务化混合部署功能，可通过Linux shell命令行访问zephyr，详见[多OS混合部署框架](https://openeuler.gitee.io/yocto-meta-openeuler/features/mcs.html)
- 分布式软总线能力增强，新增支持基于分布式软总线的openEuler和openHarmony设备认证和互通互联，增加南向wifi传输介质支持，详见[分布式软总线](https://openeuler.gitee.io/yocto-meta-openeuler/features/distributed_soft_bus.html)
- 安全加固指导，详见[安全加固说明](https://openeuler.gitee.io/yocto-meta-openeuler/security_hardening/index.html)
- 基于Preempt-RT的软实时，详见[软实时系统介绍](https://openeuler.gitee.io/yocto-meta-openeuler/features/preempt_rt.html)

## 构建系统

- 优化后的openEuler Embedded构建体系, 详见[快速构建指导](./快速构建指导.html)
- 容器化构建，增加构建专用NativeSDK，详见[容器构建指导](./容器构建指导.html)