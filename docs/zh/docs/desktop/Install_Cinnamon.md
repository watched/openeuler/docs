# 在 openEuler 上安装 Cinnamon

Cinnamon是运行在类Unix操作系统中最常用的桌面环境，也是一个功能完善、操作简单、界面友好、集使用和开发为一身的桌面环境，还是GNU计划的正式桌面。

从用户的角度看，Cinnamon是一个集成桌面环境和应用程序的套件。从开发者的角度看，Cinnamon是一个应用程序开发框架(由数目众多的实用函数库组成)。即使用户不运行Cinnamon桌面环境，用Cinnamon编写的应用程序也可以正常运行。

Cinnamon既包含文件管理器，应用商店，文本编辑器等基础软件，也包含系统采样分析，系统日志，软件工程IDE，web浏览器，简洁虚拟机监视器，开发者文档浏览器等高级应用和工具。

安装时，建议新建一个管理员用户。

##1. 配置源并更新系统
[下载](https://openeuler.org/zh/download/) openEuler ISO镜像并安装系统，更新软件源（需要配置Everything源，以及EPOL源，如下命令是在最小化安装系统的情况下安装Cinnamon）

```
sudo dnf update
```

##2. 安装字库

```
sudo dnf install dejavu-fonts liberation-fonts gnu-*-fonts google-*-fonts
```

##3. 安装Xorg

```
sudo dnf install xorg-*
```
安装时，可能会安装无用的包，可使用如下命令安装必要的xorg相关包。

```
sudo dnf install xorg-x11-apps xorg-x11-drivers xorg-x11-drv-ati \
	xorg-x11-drv-dummy xorg-x11-drv-evdev xorg-x11-drv-fbdev xorg-x11-drv-intel \
	xorg-x11-drv-libinput xorg-x11-drv-nouveau xorg-x11-drv-qxl \
	xorg-x11-drv-synaptics-legacy xorg-x11-drv-v4l xorg-x11-drv-vesa \
	xorg-x11-drv-vmware xorg-x11-drv-wacom xorg-x11-fonts xorg-x11-fonts-others \
	xorg-x11-font-utils xorg-x11-server xorg-x11-server-utils xorg-x11-server-Xephyr \
	xorg-x11-server-Xspice xorg-x11-util-macros xorg-x11-utils xorg-x11-xauth \
	xorg-x11-xbitmaps xorg-x11-xinit xorg-x11-xkb-utils
```

##4. 安装Cinnamon及组件

```
sudo dnf install cinnamon cinnamon-control-center cinnamon-desktop \
	cinnamon-menus cinnamon-screensaver cinnamon-session \
	cinnamon-settings-daemon  cinnamon-themes cjs \
	nemo nemo-extensions  muffin cinnamon-translations inxi \
	perl-XML-Dumper xapps mint-x-icons mint-y-icons mintlocale \
	python3-plum-py caribou mozjs78 python3-pam \
	python3-tinycss2 python3-xapp tint2 gnome-terminal \
	lightdm lightdm-gtk
```

##5. 开机自动启动登录管理器

```
sudo systemctl enable lightdm
```

##6. 设置系统默认以图形界面登录

```
sudo systemctl set-default graphical.target
```
重启验证

```
sudo reboot
```

