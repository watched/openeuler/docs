# SSH协议栈

## 概述

openSSH组件是以C语言的openSSL的libcrypto为基础，实现的安全外壳协议（Secure Shell）组件。主要功能为远程登录系统，保证非安全网络环境中信息加密完整可靠。openEuler提供的SSH的服务端和客户端配置项中涉及密钥交换、公钥认证、对称加密和完整性认证的配置参数取值可以选择为商密算法套件（包含SM2/3/4算法）。

## 前置条件

openssh大于等于8.8p1-5版本：

```
$ rpm -qa | grep openssh
openssh-8.8p1-5.oe2209.x86_64
openssh-server-8.8p1-5.oe2209.x86_64
openssh-clients-8.8p1-5.oe2209.x86_64
```

## 如何使用

### 场景1：用户远程登录

1. 客户端调用ssh-keygen生成用户密钥，默认保存为“\~/.ssh/id_sm2”和“\~/.ssh/id_sm2.pub”，将“\~/.ssh/id_sm2.pub”发送给服务端（也可使用ssh-copy-id命令发送）：

```
$ ssh-keygen -t sm2 -m PEM
```

2. 服务端调用ssh-keygen生成主机密钥，并将客户端发送的公钥加入授权密钥文件列表（如使用ssh-copy-id命令传输，则会自动写入）：

```
$ ssh-keygen -t sm2 -m PEM -f /etc/ssh/ssh_host_sm2_key
$ cat /path/to/id_sm2.pub >> ~/.ssh/authorized_keys
```

3. 服务端修改/etc/ssh/sshd_config，配置支持商密算法登录。商密的配置项可选商密参数如下表：

| 配置项含义               | 配置项参数                  | 配置项参数的商密取值    |
|---------------------|------------------------|---------------|
| 主机密钥公钥认证密钥 （仅服务端可配） | HostKeyAlgorithms      | /etc/ssh/ssh_host_sm2_key           |
| 主机密钥公钥认证算法          | HostKeyAlgorithms      | sm2           |
| 密钥交换算法              | KexAlgorithms          | sm2-sm3       |
| 对称加密算法              | Ciphers                | sm4-ctr       |
| 完整性校验算法             | MACs                   | hmac-sm3      |
| 用户公钥认证算法            | PubkeyAcceptedKeyTypes | sm2           |
| 用户公钥认证密钥（仅客户端可配）    | IdentityFile           | ~/.ssh/id_sm2 |
| 打印密钥指纹使用的哈希算法       | FingerprintHash        | sm3           |

4. 客户端配置商密算法完成登录。客户端可以使用命令行方式或者修改配置文件的方式使能商密算法套件。使用命令行登录方式如下：

```
ssh -o PreferredAuthentications=publickey -o HostKeyAlgorithms=sm2 -o PubkeyAcceptedKeyTypes=sm2 -o Ciphers=sm4-ctr -o MACs=hmac-sm3 -o KexAlgorithms=sm2-sm3 -i ~/.ssh/id_sm2 [remote-ip]
```
