# 安装 secGear


## 操作系统

openEuler 21.03、openEuler 20.03 LTS SP2 或更高版本

## CPU 架构

#### x86_64

需要有 Intel SGX ( Intel Software Guard Extensions ) 功能

#### AArch64

   - 硬件要求

      | 项目   | 版本                                          |
      | ------ | --------------------------------------------- |
      | 服务器 | TaiShan 200 服务器（型号 2280），仅限双路服务器 |
      | 主板   | 鲲鹏主板                                      |
      | BMC    | 1711 单板（型号 BC82SMMAB）                     |
      | CPU    | 鲲鹏 920 处理器（型号 7260、5250、5220）         |
      | 机箱   | 不限，建议 8 盘或 12 盘                           |

      要求运行的泰山服务器已经预置了 TrustZone 特性，即预装 iTrustee 安全 OS 以及配套的 BMC、BIOS 固件。

   - 运行环境要求

      CA（Client Application）应用需要 REE（Rich Execution Environment）侧 patch 才能实现与 TEE（Trusted Execution Environment）侧的 TA（Trusted Application）应用通信，在安装 secGear 前，请先搭建运行环境，操作如下：

      1. [搭建 TA/CA 应用运行环境](https://support.huaweicloud.com/dpmg-tz-kunpengcctrustzone/kunpengtrustzone_04_0007.html#:~:text=%E6%90%AD%E5%BB%BA%3E%20%E6%90%AD%E5%BB%BA%E6%AD%A5%E9%AA%A4-,%E6%90%AD%E5%BB%BA%E6%AD%A5%E9%AA%A4,-%E6%9B%B4%E6%96%B0%E6%97%B6%E9%97%B4%EF%BC%9A2022)

      2. 申请调测环境TA应用开发者证书

         a. [生成 configs.xml 文件](https://support.huaweicloud.com/dpmg-tz-kunpengcctrustzone/kunpengtrustzone_04_0009.html#:~:text=%E5%88%86%E4%BA%AB-,%E7%94%9F%E6%88%90configs.xml%E6%96%87%E4%BB%B6,-%E6%A0%B9%E6%8D%AEmanifest.txt)

         b. [申请 TA 开发者证书](https://support.huaweicloud.com/dpmg-tz-kunpengcctrustzone/kunpengtrustzone_04_0009.html#:~:text=%E5%86%85%E5%AE%B9%E4%BA%88%E4%BB%A5%E6%9B%BF%E6%8D%A2%E3%80%82-,TA%E5%BC%80%E5%8F%91%E8%80%85%E8%AF%81%E4%B9%A6%E7%94%B3%E8%AF%B7,-%E7%94%9F%E6%88%90%E6%9C%AC%E5%9C%B0%E5%AF%86)

## 安装指导

使用 secGear 机密计算编程框架，需要安装 secGear、secGear-devel 开发包。安装前，请确保已经配置了openEuler yum 源。

1. 使用 root 权限，安装 secGear 组件，参考命令如下：

   ```shell
   #yum install secGear
   #yum install secGear-devel
   ```

2. 查看安装是否成功。参考命令如下，若回显有对应软件包，表示安装成功：

   ```shell
   #rpm -q secGear 
   #rpm -q secGear-devel
   ```